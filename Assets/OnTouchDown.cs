﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class OnTouchDown : MonoBehaviour
{
    //void Update()
    //{
    //    // Code for OnMouseDown in the iPhone. Unquote to test.
    //    RaycastHit hit = new RaycastHit();
    //    for (int i = 0; i < Input.touchCount; ++i)
    //    {
    //        if (Input.GetTouch(i).phase.Equals(TouchPhase.Began))
    //        {
    //            // Construct a ray from the current touch coordinates
    //            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
    //            if (Physics.Raycast(ray, out hit))
    //            {
    //                hit.transform.gameObject.SendMessage("OnMouseDown");
    //            }
    //        }
    //    }
    //}
    Camera cam;
    Ray ray;
    LineRenderer lineren;
    RawImage raw;

    void Start()
    {
        raw = GameObject.Find("CameraTex").GetComponent<RawImage>();
        cam = GetComponent<Camera>();
        lineren = GetComponent<LineRenderer>();
    }

    //void Update()
    //{
    //    // Code for OnMouseDown in the iPhone. Unquote to test.
    //    RaycastHit hit = new RaycastHit();
        
            
    //    // Construct a ray from the current touch coordinates
    //    Ray ray = Camera.main.ScreenPointToRay(new Vector3(cam.pixelWidth/2, cam.pixelHeight/2, 0));
    //    Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
    //    if (Physics.Raycast(ray, out hit))
    //    {
    //        hit.transform.gameObject.SendMessage("OnMouseDown");
    //    }
    //}

    void Update()
    {
        //raw = GameObject.Find("CameraTex").GetComponent<RawImage>();
        // Code for OnMouseDown in the iPhone. Unquote to test.
        RaycastHit hit = new RaycastHit();
        ray = Camera.main.ScreenPointToRay(new Vector3(cam.pixelWidth / 2, cam.pixelHeight / 2, 0));

        bool hitB = Physics.Raycast(ray, out hit);       
        //Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
        if (hitB)
        {
            //lineren.SetPosition(0, ray.origin);
            //lineren.SetPosition(1, hit.point);
            if (Input.GetMouseButtonDown(0))
            {
                hit.transform.gameObject.SendMessage("OnMouseDown");
            }
        }
        FINGER[] detected = GameObject.Find("CameraTex").GetComponent<PlayMovieTextureOnUI>().detected;
        for (int i = 0; i < 10; i++)
        {
            if (detected[i].key_pushed)
            {
                ray = Camera.main.ScreenPointToRay(new Vector3(detected[i].x - 170, detected[i].y + 210, 0));
                //ray = new Ray(cam.transform.position, new Vector3(detected[i].x, detected[i].y, Vector3.Distance(raw.transform.position, cam.transform.position)) - cam.transform.position);
                hitB = Physics.Raycast(ray, out hit);
                lineren.SetPosition(0, ray.origin);
                lineren.SetPosition(1, hit.point);
                if (hitB)
                {
                    hit.transform.gameObject.SendMessage("OnMouseDown");
                }
            }
        }
    }
}