﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PianoKeyScript : MonoBehaviour {

    private AudioSource audiosrc;
    public float semitone_offset = 0;
    // Use this for initialization
    void Start () {
        audiosrc = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnMouseDown()
    {
        PlayNote();
    }

    void PlayNote()
    {
        audiosrc.pitch = Mathf.Pow(2f, semitone_offset / 12.0f);
        audiosrc.Play();
    }
}
