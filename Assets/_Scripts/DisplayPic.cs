﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayPic : MonoBehaviour {
    private RawImage rawimage;


    void Start()
    {
        
        rawimage = GetComponent<RawImage>();
        rawimage.rectTransform.sizeDelta = new Vector2(Screen.width, Screen.height);

        WebCamTexture webcamTexture = new WebCamTexture();
        rawimage.texture = webcamTexture;
        rawimage.material.mainTexture = webcamTexture;
        webcamTexture.Play();
    }

    void Update()
    {

    }
}
