﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchToCalib : MonoBehaviour {

	// Use this for initialization
	void Start () {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Main scene"));
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnMouseDown()
    {
        //SceneManager.UnloadSceneAsync("Main scene");
        //SceneManager.SetActiveScene(SceneManager.GetSceneByName("Calibration"));
        SceneManager.LoadScene("Calibration");
    }
}
