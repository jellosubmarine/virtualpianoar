﻿using UnityEngine;
 
public class PositionAtFaceScreenSpace : MonoBehaviour
{
    private float _camDistance;
 
    void Start()
    {
        _camDistance = Vector3.Distance(Camera.main.transform.position, transform.position);
    }
 
    void Update()
    {
        
        if (OpenCVFaceDetection.NormalizedFacePositions.Count == 0)
            return;
        //Debug.Log(OpenCVFaceDetection.NormalizedFacePositions[0]);
        transform.position = new Vector3(transform.position.x + OpenCVFaceDetection.NormalizedFacePositions[0].x, transform.position.y + OpenCVFaceDetection.NormalizedFacePositions[0].y, transform.position.z + _camDistance);
    }
}