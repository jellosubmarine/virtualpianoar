﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CalibrationRay : MonoBehaviour
{
    Camera cam;
    Ray ray;
    LineRenderer lineren;


    void Start()
    {
        cam = GetComponent<Camera>();
        lineren = GetComponent<LineRenderer>();
    }

    void Update()
    {
        // Code for OnMouseDown in the iPhone. Unquote to test.
        RaycastHit hit = new RaycastHit();
        ray = Camera.main.ScreenPointToRay(new Vector3(cam.pixelWidth / 2, cam.pixelHeight / 2, 0));
        
        bool hitB = Physics.Raycast(ray, out hit);
        
        //Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
        if (hitB)
        {
            lineren.SetPosition(0, ray.origin);
            lineren.SetPosition(1, hit.point);
            if (Input.GetMouseButtonDown(0))
            {
                hit.transform.gameObject.SendMessage("OnMouseDown");
            }
        }
    }
}