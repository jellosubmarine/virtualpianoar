﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SetSceneToMain : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnMouseDown()
    {
        //SceneManager.UnloadSceneAsync("Calibration");
        //SceneManager.SetActiveScene(SceneManager.GetSceneByName("Main scene"));
        SceneManager.LoadScene("Main scene");

    }
}
