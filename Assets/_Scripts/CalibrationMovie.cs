﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using System;

//internal static class OPENCV
//{
//    [DllImport("libnative-lib", CallingConvention = CallingConvention.Cdecl)]
//    internal static extern IntPtr ProcessFrame(IntPtr img);


//}

////internal static class OPENCV
////{
////    [DllImport("OpenCVunity", CallingConvention = CallingConvention.Cdecl)]
////    internal static extern IntPtr ProcessFrame(IntPtr img);


////}
//internal static class BYTEOP
//{
//    internal static byte[] Color32ArrayToByteArray(Color32[] colors)
//    {
//        if (colors == null || colors.Length == 0)
//            return null;

//        int lengthOfColor32 = Marshal.SizeOf(typeof(Color32));
//        int length = lengthOfColor32 * colors.Length;
//        byte[] bytes = new byte[length];

//        GCHandle handle = default(GCHandle);
//        try
//        {
//            handle = GCHandle.Alloc(colors, GCHandleType.Pinned);
//            IntPtr ptr = handle.AddrOfPinnedObject();
//            Marshal.Copy(ptr, bytes, 0, length);
//        }
//        finally
//        {
//            if (handle != default(GCHandle))
//                handle.Free();
//        }

//        return bytes;
//    }
//}

public class CalibrationMovie : MonoBehaviour
{
    private WebCamTexture webcamTexture;
    private RawImage rawimage;
    public int camWidth = 0, camHeight = 0;
    private Color32[] data;
    private byte[] byteData;
    private byte[] imgData;
    private Texture2D tex;
    private int channels = 4;

    public bool calibration = false;
    public bool calibReset = false;


    void Start()
    {
        //OPENCV.Initialize(ref camWidth, ref camHeight);

        //rawimage.texture = webcamTexture;
        //rawimage.material.mainTexture = webcamTexture;
        //webcamTexture.Play();

        rawimage = GetComponent<RawImage>();
        rawimage.rectTransform.sizeDelta = new Vector2(Screen.width / 2, Screen.height);

        webcamTexture = new WebCamTexture(camWidth, camHeight);
        //rawimage.texture = webcamTexture;
        //rawimage.material.mainTexture = webcamTexture;
        webcamTexture.Play();
        data = new Color32[webcamTexture.width * webcamTexture.height];
        imgData = new byte[channels * webcamTexture.width * webcamTexture.height];

        //RGBA32 or RGBA24
        tex = new Texture2D(camWidth, camHeight, TextureFormat.RGBA32, false);

    }

    void Update()
    {
        webcamTexture.GetPixels32(data);

        unsafe
        {
            byteData = BYTEOP.Color32ArrayToByteArray(data);
            fixed (byte* outData = byteData)
            {
                IntPtr ptr = (IntPtr)outData;
                if (calibration)
                {
                    if (calibReset)
                    {
                        OPENCV.FirstCalibrate(ptr);
                        //OPENCV.ResetThreshold();
                        calibReset = false;
                    }
                    else
                    {
                        OPENCV.Calibrate(ptr);
                    }
                    calibration = false;
                }
                IntPtr imgptr = (IntPtr)OPENCV.ProcessFrame(ptr);
                
                Marshal.Copy(imgptr, imgData, 0, channels * webcamTexture.width * webcamTexture.height);
            }
        }


        //byte[] imgData = OPENCV.ProcessFrame();
        //if (imgData.Length != 0)
        //{

        tex.LoadRawTextureData(imgData);
        tex.Apply();
        rawimage.texture = tex;
        //}
    }

}
