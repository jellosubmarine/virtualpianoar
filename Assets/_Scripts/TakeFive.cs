﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class TakeFive : MonoBehaviour {

    // Use this for initialization
	void Start () {
        GetComponent<TextMesh>().text = test.Five().ToString();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

internal static class test
{
    [DllImport("libnative-lib")]
    internal static extern int Five();
   
}