﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//internal static class OPENCV2 {
//    [DllImport("libnative-lib", CallingConvention = CallingConvention.Cdecl)]
//    internal static extern void ResetThreshold();
//}

public class ResetScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnMouseDown()
    {
        GameObject.Find("CameraTex").GetComponent<CalibrationMovie>().calibReset = true;
    }
}
