﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using System;

[StructLayout(LayoutKind.Sequential, Size = 13)]
public struct FINGER
{
    public int x;
    public int y;
    public bool key_pushed;
    public int ID;
}

internal static class OPENCV
{
    [DllImport("libnative-lib", CallingConvention = CallingConvention.Cdecl)]
    internal static extern IntPtr ProcessFrame(IntPtr img);

    [DllImport("libnative-lib", CallingConvention = CallingConvention.Cdecl)]
    internal static extern void Calibrate(IntPtr img);

    [DllImport("libnative-lib", CallingConvention = CallingConvention.Cdecl)]
    internal static extern void ResetThreshold();

    [DllImport("libnative-lib", CallingConvention = CallingConvention.Cdecl)]
    internal static extern void FirstCalibrate(IntPtr img);

    [DllImport("libnative-lib", CallingConvention = CallingConvention.Cdecl)]
    internal unsafe static extern IntPtr ReturnContours(IntPtr img, IntPtr detected);
}

//internal static class OPENCV
//{
//    [DllImport("OpenCVunity", CallingConvention = CallingConvention.Cdecl)]
//    internal static extern IntPtr ProcessFrame(IntPtr img);


//}
internal static class BYTEOP
{
    internal static byte[] Color32ArrayToByteArray(Color32[] colors)
    {
        if (colors == null || colors.Length == 0)
            return null;

        int lengthOfColor32 = Marshal.SizeOf(typeof(Color32));
        int length = lengthOfColor32 * colors.Length;
        byte[] bytes = new byte[length];

        GCHandle handle = default(GCHandle);
        try
        {
            handle = GCHandle.Alloc(colors, GCHandleType.Pinned);
            IntPtr ptr = handle.AddrOfPinnedObject();
            Marshal.Copy(ptr, bytes, 0, length);
        }
        finally
        {
            if (handle != default(GCHandle))
                handle.Free();
        }

        return bytes;
    }
}




public class PlayMovieTextureOnUI : MonoBehaviour
{
    private WebCamTexture webcamTexture;
    public RawImage rawimage;
    public int camWidth = 0, camHeight = 0;
    private Color32[] data;
    private byte[] byteData;
    private byte[] imgData;
    private Texture2D tex;
    private int channels = 4;

    public FINGER[] detected;
    void Start()
    {
        //OPENCV.Initialize(ref camWidth, ref camHeight);

        //rawimage.texture = webcamTexture;
        //rawimage.material.mainTexture = webcamTexture;
        //webcamTexture.Play();

        rawimage = GetComponent<RawImage>();
        rawimage.rectTransform.sizeDelta = new Vector2(Screen.width/2, Screen.height/2);

        webcamTexture = new WebCamTexture(camWidth,camHeight);
        //rawimage.texture = webcamTexture;
        //rawimage.material.mainTexture = webcamTexture;
        webcamTexture.Play();
        data = new Color32[webcamTexture.width * webcamTexture.height];
        imgData = new byte[channels * webcamTexture.width * webcamTexture.height];

        //RGBA32 or RGBA24
        tex = new Texture2D(camWidth, camHeight, TextureFormat.RGBA32, false);
        detected = new FINGER[10];

    }

    void Update()
    {
        webcamTexture.GetPixels32(data);
        
        unsafe
        {
            byteData = BYTEOP.Color32ArrayToByteArray(data);
            fixed (byte* outData = byteData)
            {
                fixed (FINGER* detectedFingers = detected)
                {
                    IntPtr ptr = (IntPtr)outData;
                    IntPtr ptr2 = (IntPtr)detectedFingers;
                    IntPtr imgptr = (IntPtr)OPENCV.ReturnContours(ptr, ptr2);
                    Marshal.Copy(imgptr, imgData, 0, channels * webcamTexture.width * webcamTexture.height);
                }
            }
        }
        

        //byte[] imgData = OPENCV.ProcessFrame();
        //if (imgData.Length != 0)
        //{
               
        tex.LoadRawTextureData(imgData);
        tex.Apply();
        rawimage.texture = tex;
        //}
    }

}

